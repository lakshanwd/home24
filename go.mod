module home-24

go 1.16

require (
	github.com/go-chi/chi/v5 v5.0.4
	github.com/go-chi/render v1.0.1
	golang.org/x/net v0.0.0-20210903162142-ad29c8ab022f
)
